# Stream Collector Utilities

A **really** small collection of Java stream Collectors. At the moment there are only five of them. The code is part of
two blog articles about Collectors (
[Einsammeln und portionieren mit Stream Collector](https://schegge.de/2019/03/09/einsammeln-und-portionieren/),
[Sortieren auf Teufel komm raus](https://schegge.de/2020/03/sortieren-auf-teufel-komm-raus/) and
[Optionales einsammeln](https://schegge.de/2019/03/09/optionales-einsammeln/)
)

## Content

- [Portion Collectors](#portion-collectors)
- [Optionals Collectors](#optionals-collectors)
- [Enumerated Collector](#enumerated-collector)
- [Ellipsis Collector](#ellipsis-collector)
- [Ranges Collector](#ranges-collector)
- [Various Filters](#various-filters)
- [Various Comparators](#various-comparators)

## Portion collectors

The PortionCollector groups stream elements into portions of a defined size.

### PortionCollector.toLists(portionSize)

This PortionCollector groups stream elements in List instances.

```java
Collection<List<String>>result=IntStream.range(0,20).mapToObj(x->"#"+x).collect(toLists(4));
System.out.println(result);
```

```
[[#0, #1, #2, #3], [#4, #5, #6, #7], [#8, #9, #10, #11], [#12, #13, #14, #15], [#16, #17, #18, #19]]
```

### PortionCollector.toSets(portionSize)

This PortionCollector groups stream elements in Set instances.

```java
Collection<Set<String>>result=IntStream.range(0,20).mapToObj(x->"#"+x).collect(toSets(4));
System.out.println(result);
```

```
[[#0, #1, #2, #3], [#4, #5, #6, #7], [#8, #9, #10, #11], [#12, #13, #14, #15], [#16, #17, #18, #19]]
```

## Optionals collectors

The `OptionalsCollector` wrappes other `Collector`s to collect the content of `Optional`s from a stream.

### OptionalsCollectors.withEmpty(Collector wrapped)

This `Collector` replaces empty `Optional`s by null.

```java
List<Optional<String>>list=asList(Optional.of("test1"),Optional.of("test2"),Optional.empty(),Optional.of("test2"),Optional.empty());
Set<String> values=list.stream().collect(OptionalsCollector.withEmpty(toList()));
System.out.println(values);
```    

```
[test1, test2, null, test2, null]
```

### OptionalsCollectors.withoutEmpty(Collector wrapped)

This `Collector` drops empty `Optional`s.

```java
List<Optional<String>>list=asList(Optional.of("test1"),Optional.of("test2"),Optional.empty(),Optional.of("test2"),Optional.empty());
Set<String> values=list.stream().collect(OptionalsCollector.withoutEmpty(toSet()));
System.out.println(values);
```    

```
[test1, test2]
```

## Extrema collector

The `ExtremaCollector` collects minimum and maximum values from a `Stream`.

```java
List<Person> list=personRepository.findAll();
Extrema<Person> extremaByBirthday=list.stream().collect(ExtremaCollector.extrema(Comparator.comparing(Person::getBirthDay)));
System.out.println(extremaByBirthday.getMin().map(Person::getBirthDay));
System.out.println(extremaByBirthday.getMax().map(Person::getBirthDay));
```    

```
1968-08-24
2007-06-25
```

## Enumerated collector

The `EnumeratedCollector` collects String values from a `Stream`. The last value is appended with a special delimiter.

### Enumerated collector with default (comma separated, last value added with "und")

```java
List<String> maxBrothers=List.of("Chico","Harpo","Groucho","Gummo","Zeppo");
System.out.println(maxBrothers.stream().collect(EnumeratedCollector.enumerated()));
```

```
Chico, Harpo, Groucho, Gummo und Zeppo
```

### Enumerated collector with last delimiter

```java
List<String> directions=List.of("North","East","South","West");
System.out.println(maxBrothers.stream().collect(EnumeratedCollector.enumerated(" or ")));
```

```
North, East, South or West
```

### Prefixed enumerated collector with last delimiter

```java
List<String> directions=List.of("North","East","South","West");
System.out.println(directions.stream().collect(EnumeratedCollector.prefixedEnumerated("Direction: "," or ")));
```

```
Direction: North, East, South or West
```

## Ellipsis collector

The `EllipsisCollector` collects String values from a `Stream`. The collector appends elements until `maxLength` is
reached. For missing elements an ellipsis (by default `...`) is added.

### Ellipsis collector with default (comma separated, ellipsis "...")

```java
List<String> maxBrothers=List.of("Chico","Harpo","Groucho","Gummo","Zeppo");
System.out.println(maxBrothers.stream().collect(EllipsisCollector.ellipsis(16)));
```

```
Chico, Harpo, ...
```

### Ellipsis Collector with custom separator and custom ellipsis

```java
List<String> maxBrothers=List.of("Chico","Harpo","Groucho","Gummo","Zeppo");
System.out.println(maxBrothers.stream().collect(EllipsisCollector.ellipsis(>>>",24)));
```

```
Chico;Harpo;Groucho;>>>
```

## Ranges Collector

### Ranges Collector (RANGE)

```java
List<Integer> numbers=List.of(101,102,103,105,106,107,108,110);
System.out.println(numbers.stream().collect(RangesCollector.ranges()));
System.out.println(numbers.stream().collect(RangesCollector.ranges(Compactness.RANGE)));
```

```
101-103,105-107,108,110
101-103,105-107,108,110
```

### Ranges Collector (OFFSET)

```java
List<Integer> numbers=List.of(101,102,103,105,106,107,108,110);
System.out.println(numbers.stream().collect(RangesCollector.ranges(Compactness.OFFSET)));
```

```
101+2,105+2,108,110
```

### Ranges Collector (RANGE_OFFSET)

```java
List<Integer> numbers=List.of(101,102,103,105,106,107,108,110);
System.out.println(numbers.stream().collect(RangesCollector.ranges(Compactness.RANGE_OFFSET)));
```

```
101+2,+2+2,+2,+2
```

## Various filter

### Filters.nonEmpty attribute filter

Matches all entries with not empty attribute in a `Stream`.

```java
List<Person> list=personRepository.findAll();
Set<String> values=list.stream().filter(Filters.nonEmpty(Person::getTitle)).collect(toSet()));
System.out.println(values);
```    

```
[Dr. Mabuse]
```

### Filters.isEmpty attribute filter

Matches all entries with empty attribute in a `Stream`.

```java
List<Person> list=personRepository.findAll(); // Ed, Jens Kaiser, Dr. Mabuse
Set<String> values=list.stream().filter(Filters.isEmpty(Person::getTitle)).collect(toSet()));
System.out.println(values);
```    

```
[Ed, Jens Kaiser]
```

### Filters.isNull attribute filter

Matches all entries with `null` attribute in a `Stream`.

```java
List<Person> list=personRepository.findAll(); // Ed, Jens Kaiser, Dr. Mabuse
Set<String> values=list.stream().filter(Filters.isNull(Person::getDeath)).collect(toSet()));
System.out.println(values);
```    

```
[Dr. Mabuse, Jens Kaiser]
```

### Filters.nonNull attribute filter

Matches all String with non- attribute in a `Stream`.

```java
List<Person> list=personRepository.findAll(); // Ed, Jens Kaiser, Dr. Mabuse
Set<String> values=list.stream().filter(Filters.nonNull(Person::getDeath)).collect(toSet()));
System.out.println(values);
```

```
[Ed] // Ed Is Dead -- Pixies
```

### Filters.nonEmpty String filter

Matches all not empty `Strings` in a `Stream`.

```java
Set<String> values=Stream.of("123", "", "abc", "  ", "xxx").filter(Filters.noEmpty()).collect(toSet()));
System.out.println(values);
```

```
123, abc,   , xxx
```

### Filters.nonBlank String filter

Matches all not blank `Strings` in a `Stream`.

```java
Set<String> values=Stream.of("123", "", "abc", "  ", "xxx").filter(Filters.nonBlank()).collect(toSet()));
System.out.println(values);
```

```
123, abc, xxx
```

## Various Comparator

### Comparators.safeComparing

null save comparator.

```java
persons.stream().sorted(Comparators.saveComparing(Person::getDeath)).collect(toList());
```

### Comparators.safeComparing with custom comparator

null save comparator with custom comparator.

```java
persons.stream().sorted(Comparators.saveComparing(Person::getName,myNameComparator)).collect(toList());
```

### Comparators.safeComparing with default value

null save comparator with default value for null

```java
cars.stream().sorted(Comparators.saveComparing(Cars::getColor,"black")).collect(toList());
```

### Comparators.nullAwareLeft

sort null to left side

```java
persons.stream().sorted(Comparators.nullAwareLeft(Person::getDeath)).collect(toList());
```

### Comparators.nullAwareRight

sort null to the right side

```java
persons.stream().sorted(Comparators.nullAwareRight(Person::getDeath)).collect(toList());
```
