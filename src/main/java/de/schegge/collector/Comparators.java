package de.schegge.collector;

import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static java.util.Objects.requireNonNull;

import java.util.Comparator;
import java.util.function.Function;

public final class Comparators {

  private Comparators() {
    super();
  }

  public static <T, U extends Comparable<? super U>> Comparator<T> safeComparing(
      Function<? super T, ? extends U> keyExtractor) {
    return safeComparingLeft(keyExtractor, naturalOrder());
  }

  public static <T, U extends Comparable<? super U>> Comparator<T> safeComparingLeft(
      Function<? super T, ? extends U> keyExtractor, Comparator<? super U> comparator) {
    return comparing(keyExtractor, nullAwareLeft(comparator));
  }

  public static <T, U extends Comparable<? super U>> Comparator<T> safeComparingRight(
      Function<? super T, ? extends U> keyExtractor, Comparator<? super U> comparator) {
    return comparing(keyExtractor, nullAwareRight(comparator));
  }

  public static <T, U extends Comparable<? super U>> Comparator<T> safeComparing(
      Function<? super T, ? extends U> keyExtractor, U defaultCompareValue) {
    return comparing(safeKeyExtractor(requireNonNull(keyExtractor), requireNonNull(defaultCompareValue)));
  }

  private static <T, U extends Comparable<? super U>> Function<? super T, ? extends U> safeKeyExtractor(
      Function<? super T, ? extends U> keyExtractor, U defaultKey) {
    return keyExtractor.andThen(x -> x != null ? x : defaultKey);
  }

  public static <T> Comparator<T> nullAwareLeft(Comparator<? super T> comparator) {
    return nullAware(comparator, -1, 1);
  }

  public static <T> Comparator<T> nullAwareRight(Comparator<? super T> comparator) {
    return nullAware(comparator, 1, -1);
  }

  private static <T> Comparator<T> nullAware(Comparator<? super T> comparator, int left, int right) {
    return (c1, c2) -> {
      int i1 = c1 == null ? left : 0;
      int i2 = c2 == null ? right : 0;
      return i1 == i2 ? comparator.compare(c1, c2) : i1 + i2;
    };
  }
}
