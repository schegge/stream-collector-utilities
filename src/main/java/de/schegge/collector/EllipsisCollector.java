package de.schegge.collector;

import java.util.StringJoiner;
import java.util.stream.Collector;

public final class EllipsisCollector {
  private EllipsisCollector() {
    super();
  }

  private static class Accumulator {

    private final StringJoiner joiner;
    private final String ellipsis;
    private final int maxLength;
    private final CharSequence delimiter;

    public Accumulator(CharSequence delimiter, String ellipsis, int maxLength) {
      joiner = new StringJoiner(delimiter);
      this.delimiter = delimiter;
      this.ellipsis = ellipsis;
      this.maxLength = maxLength - ellipsis.length() - delimiter.length();
    }

    public void add(CharSequence b) {
      if (joiner.length() > maxLength) {
        return;
      }
      joiner.add(b);
    }

    public String get() {
      return joiner.length() < maxLength ? joiner.toString() : joiner + delimiter.toString() + ellipsis;
    }
  }

  public static Collector<CharSequence, Accumulator, String> ellipsis(int maxLength) {
    return ellipsis(", ", "…", maxLength);
  }

  public static Collector<CharSequence, Accumulator, String> ellipsis(CharSequence delimiter, String ellipsis, int maxLength) {
    return Collector.of(() -> new Accumulator(delimiter, ellipsis, maxLength), Accumulator::add, (a, b) -> {
      throw new UnsupportedOperationException("parallel not supported");
    }, Accumulator::get);
  }
}
