package de.schegge.collector;

import java.util.EnumMap;
import java.util.Map;

public final class EnumUtil {

  private EnumUtil() {
    super();
  }

  public static <E extends Enum<E>, F extends Enum<F>> Map<F, E> mapping(Class<F> keyType, Class<E> valueType) {
    return getFeMap(keyType, keyType.getEnumConstants(), valueType.getEnumConstants());
  }

  public static <E extends Enum<E>, F extends Enum<F>> Map<F, E> mapping(Class<F> keyType, E... values) {
    return getFeMap(keyType, keyType.getEnumConstants(), values);
  }

  private static <E extends Enum<E>, F extends Enum<F>> Map<F, E> getFeMap(Class<F> keyType, F[] keys, E[] values) {
    if (values.length != keys.length) {
      throw new IllegalArgumentException("enum values does not match");
    }
    Map<F, E> enumMap = new EnumMap<>(keyType);
    for (int i = 0; i < keys.length; i++) {
      enumMap.put(keys[i], values[i]);
    }
    return enumMap;
  }
}
