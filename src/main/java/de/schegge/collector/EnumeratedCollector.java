package de.schegge.collector;

import java.util.StringJoiner;
import java.util.stream.Collector;

public final class EnumeratedCollector {

  private EnumeratedCollector() {
    super();
  }

  private static class Accumulator {

    private final StringJoiner joiner;
    private String last;
    private final String lastDelimiter;

    public Accumulator(String delimiter, String lastDelimiter, String prefix) {
      joiner = new StringJoiner(delimiter, prefix, "");
      joiner.setEmptyValue("");
      this.lastDelimiter = lastDelimiter;
    }

    public void add(CharSequence b) {
      if (last != null) {
        joiner.add(last);
      }
      last = String.valueOf(b);
    }

    public String get() {
      if (last == null) {
        return "";
      }
      if (joiner.length() == 0) {
        return joiner.add(last).toString();
      }
      return joiner + lastDelimiter + last;
    }
  }

  public static Collector<CharSequence, Accumulator, String> enumerated() {
    return enumerated(" und ", ", ");
  }

  public static Collector<CharSequence, Accumulator, String> enumerated(String lastDelimiter) {
    return enumerated(lastDelimiter, ", ");
  }

  public static Collector<CharSequence, Accumulator, String> enumerated(String lastDelimiter, String delimiter) {
    return enumerated("", lastDelimiter, delimiter);
  }

  public static Collector<CharSequence, Accumulator, String> enumerated(String prefix, String lastDelimiter,
      String delimiter) {
    return Collector.of(() -> new Accumulator(delimiter, lastDelimiter, prefix), Accumulator::add, (a, b) -> {
      throw new UnsupportedOperationException("parallel not supported");
    }, Accumulator::get);
  }

  public static Collector<CharSequence, Accumulator, String> prefixedEnumerated(String prefix) {
    return enumerated(prefix, " und ", ", ");
  }

  public static Collector<CharSequence, Accumulator, String> prefixedEnumerated(String prefix, String lastDelimiter) {
    return enumerated(prefix, lastDelimiter, ", ");
  }
}
