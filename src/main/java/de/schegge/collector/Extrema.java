package de.schegge.collector;

import java.util.Optional;

public class Extrema<T> {

  private final T min;
  private final T max;

  Extrema(T min, T max) {
    this.min = min;
    this.max = max;
  }

  public Optional<T> getMin() {
    return Optional.ofNullable(min);
  }

  public Optional<T> getMax() {
    return Optional.ofNullable(max);
  }
}
