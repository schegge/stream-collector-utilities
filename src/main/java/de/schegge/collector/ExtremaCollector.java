package de.schegge.collector;

import static java.util.stream.Collector.Characteristics.CONCURRENT;

import java.util.Comparator;
import java.util.stream.Collector;

public class ExtremaCollector {

  private ExtremaCollector() {
    super();
  }

  static class Accumulator<T> {
    T min;
    T max;
    Comparator<T> comparator;

    public Accumulator(Comparator<T> comparator) {
      this.comparator = comparator;
    }

    void add(T value) {
      if (min == null) {
        min = value;
        max = value;
        return;
      }
      if (comparator.compare(min, value) > 0) {
        min = value;
      } else if (comparator.compare(max, value) < 0) {
        max = value;
      }
    }

    Accumulator<T> merge(Accumulator<T> collector) {
      if (comparator.compare(min, collector.min) > 0) {
        min = collector.min;
      }
      if (comparator.compare(max, collector.max) < 0) {
        max = collector.max;
      }
      return this;
    }

    Extrema<T> value() {
      return new Extrema<>(min, max);
    }
  }

  public static <T> Collector<T, Accumulator<T>, Extrema<T>> extrema(Comparator<T> comparator) {
    return Collector.of(() -> new Accumulator<>(comparator),
        Accumulator::add, Accumulator::merge, Accumulator::value, CONCURRENT);
  }
}
