package de.schegge.collector;

import com.google.errorprone.annotations.InlineMe;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

public final class Filters {

    private Filters() {
        super();
    }

    public static <T, U> Predicate<T> nonNull(Function<? super T, ? extends U> keyExtractor) {
        Objects.requireNonNull(keyExtractor);
        return t -> keyExtractor.apply(t) != null;
    }

    public static <T, U> Predicate<T> isNull(Function<? super T, ? extends U> keyExtractor) {
        Objects.requireNonNull(keyExtractor);
        return t -> keyExtractor.apply(t) == null;
    }

    public static <T> Predicate<T> isEmpty(Function<? super T, String> keyExtractor) {
        Objects.requireNonNull(keyExtractor);
        return t -> keyExtractor.apply(t).isEmpty();
    }

    @InlineMe(replacement = "Filters.nonEmpty(keyExtractor)", imports = "de.schegge.collector.Filters")
    @Deprecated
    public static <T> Predicate<T> isNotEmpty(Function<? super T, String> keyExtractor) {
        return nonEmpty(keyExtractor);
    }

    public static <T> Predicate<T> nonEmpty(Function<? super T, String> keyExtractor) {
        Objects.requireNonNull(keyExtractor);
        return t -> !keyExtractor.apply(t).isEmpty();
    }

    public static Predicate<String> nonEmpty() {
        return value -> !value.isEmpty();
    }

    public static Predicate<String> nonBlank() {
        return value -> !value.isBlank();
    }

}
