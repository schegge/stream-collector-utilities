package de.schegge.collector;

import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collector.Characteristics;

public final class OptionalsCollector {
	private OptionalsCollector() {
		super();
	}

	public static <T, U, R> Collector<Optional<T>, U, R> withEmpty(Collector<T, U, R> wrapped) {
		return Collector.of(wrapped.supplier(), (x, o) -> wrapped.accumulator().accept(x, o.orElse(null)),
				wrapped.combiner(), wrapped.finisher(), wrapped.characteristics().toArray(new Characteristics[0]));
	}

	public static <T, U, R> Collector<Optional<T>, U, R> withoutEmpty(Collector<T, U, R> wrapped) {
		return Collector.of(wrapped.supplier(), (x, o) -> o.ifPresent(t -> wrapped.accumulator().accept(x, t)),
				wrapped.combiner(), wrapped.finisher(), wrapped.characteristics().toArray(new Characteristics[0]));
	}
}