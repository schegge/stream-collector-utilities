package de.schegge.collector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collector;

public class PortionsCollector {

	private PortionsCollector() {
		super();
	}

	private static class Accu<T, U extends Collection<T>> {
		private final Map<Integer, U> map = new HashMap<>();
		private final AtomicInteger counter = new AtomicInteger(0);
		private final int size;
		private final Function<? super Integer, ? extends U> function;

		public Accu(int size, Function<? super Integer, ? extends U> function) {
			this.size = size;
			this.function = function;
		}

		public void add(T b) {
			map.computeIfAbsent(counter.getAndIncrement() / size, function).add(b);
		}

		public Collection<U> get() {
			return map.values();
		}
	}

	public static <T> Collector<T, Accu<T, List<T>>, Collection<List<T>>> toLists(int size) {
		return Collector.of(() -> new Accu<>(size, ArrayList::new), Accu::add, (a, b) -> {
			throw new UnsupportedOperationException("parallel not supported");
		}, Accu::get);
	}

	public static <T> Collector<T, Accu<T, Set<T>>, Collection<Set<T>>> toSets(int size) {
		return Collector.of(() -> new Accu<>(size, HashSet::new), Accu::add, (a, b) -> {
			throw new UnsupportedOperationException("parallel not supported");
		}, Accu::get);
	}
}
