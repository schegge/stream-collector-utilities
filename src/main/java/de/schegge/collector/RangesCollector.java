package de.schegge.collector;

import java.util.StringJoiner;
import java.util.stream.Collector;

public class RangesCollector {
  public enum Compactness {
    RANGE,
    OFFSET,
    RANGE_OFFSET
  }

  private static class Accumulator {

    private final StringJoiner joiner;
    private Integer[] currentRange;
    private Integer last;
    private final Compactness compact;

    public Accumulator(Compactness compact) {
      joiner = new StringJoiner(",");
      this.compact = compact;
    }

    private String toString(Integer[] range) {
      String start = range[0].toString();
      switch (compact) {
        case OFFSET:
          if (range[1].equals(range[0])) {
            return start;
          }
          return start + "+" + (range[1] - range[0]);
        case RANGE_OFFSET:
          String lastOffset = last != null ? "+" + (range[0] - last) : start;
          if (range[1].equals(range[0])) {
            return lastOffset;
          }
          return lastOffset + "+" + (range[1] - range[0]);
        case RANGE:
        default:
          if (range[1].equals(range[0])) {
            return start;
          }
          return start + "-" + range[1];
      }
    }

    public void add(Integer value) {
      if (currentRange == null) {
        currentRange = new Integer[] { value, value };
        return;
      }
      if (currentRange[1] == value - 1) {
        currentRange[1] = value;
        return;
      }
      joiner.add(toString(currentRange));
      last = currentRange[1];
      currentRange = new Integer[] { value, value };
    }

    public String get() {
      return currentRange == null ? "" : joiner.add(toString(currentRange)).toString();
    }
  }

  public static Collector<Integer, ?, String> ranges() {
    return ranges(Compactness.RANGE);
  }

  public static Collector<Integer, ?, String> ranges(Compactness compactness) {
    return Collector.of(() -> new Accumulator(compactness), Accumulator::add, (a, b) -> {
      throw new UnsupportedOperationException("parallel not supported");
    }, Accumulator::get);
  }
}
