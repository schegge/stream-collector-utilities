package de.schegge.collector;

import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import static de.schegge.collector.Comparators.nullAwareLeft;
import static de.schegge.collector.Comparators.nullAwareRight;
import static de.schegge.collector.Comparators.safeComparing;
import static de.schegge.collector.Comparators.safeComparingLeft;
import static de.schegge.collector.Comparators.safeComparingRight;
import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsFirst;
import static java.util.Comparator.nullsLast;
import static java.util.Comparator.reverseOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ComparatorsTest {

    private record Values(Integer value) {

        @Override
            public String toString() {
                return hashCode() + " " + value;
            }
        }

    @Test
    void sortSecure() {
        List<Values> values = Stream.of(1, 2, null, 4, 5, null, 7, 8, null).map(Values::new)
                .sorted(safeComparing(Values::value)).toList();
        assertEquals(1, values.get(3).value());
    }

    @Test
    void sortSecureWithComparator() {
        Comparator<Integer> comparator = reverseOrder();
        List<Values> values = Stream.of(1, 2, null, 4, 5, null, 7, 8, null).map(Values::new)
                .sorted(safeComparingLeft(Values::value, comparator)).toList();
        assertEquals(8, values.get(3).value());
    }

    @Test
    void sortSecureWithComparatorRight() {
        Comparator<Integer> comparator = reverseOrder();
        List<Values> values = Stream.of(1, 2, null, 4, 5, null, 7, 8, null).map(Values::new)
                .sorted(safeComparingRight(Values::value, comparator)).toList();
        assertEquals(4, values.get(3).value());
    }

    @Test
    void forbiddenCalls() {
        assertThrows(NullPointerException.class, () -> safeComparing(null));
        assertThrows(NullPointerException.class, () -> safeComparing(Values::value, null));
    }

    @Test
    void sortWithNullEntriesNullFirst() {
        List<Values> values = Stream.of(1, 2, null, 4, 5, null, 7, 8, null).map(i -> i == null ? null : new Values(i))
                .sorted(nullsFirst(comparing(Values::value).thenComparing(Values::value))).toList();
        assertNull(values.get(0));
        assertNull(values.get(1));
        assertNull(values.get(2));
    }

    @Test
    void sortNullAwareWithNullEntries() {
        List<Values> values = Stream.of(1, 2, null, 4, 5, null, 7, 8, null).map(i -> i == null ? null : new Values(i))
                .sorted(nullAwareLeft(comparing(Values::value))).toList();
        assertNull(values.get(0));
        assertNull(values.get(1));
        assertNull(values.get(2));
    }

    @Test
    void sortWithNullEntriesNullLast() {
        List<Values> values = Stream.of(1, 2, null, 4, 5, null, 7, 8, null).map(i -> i == null ? null : new Values(i))
                .sorted(nullsLast(comparing(Values::value))).toList();
        assertNull(values.get(6));
        assertNull(values.get(7));
        assertNull(values.get(8));
    }

    @Test
    void sortNullAwareRightWithNullEntries() {
        List<Values> values = Stream.of(1, 2, null, 4, 5, null, 7, 8, null).map(i -> i == null ? null : new Values(i))
                .sorted(nullAwareRight(comparing(Values::value))).toList();
        assertNull(values.get(6));
        assertNull(values.get(7));
        assertNull(values.get(8));
    }

    @Test
    void sortSecureReversed() {
        List<Values> values = Stream.of(1, 2, null, 4, 5, null, 7, 8, null).map(Values::new)
                .sorted(safeComparing(Values::value).reversed()).toList();
        assertEquals(4, values.get(3).value());
    }

    @Test
    void sortSecureWithHighDefaultValue() {
        List<Values> values = Stream.of(1, 2, null, 4, 5, null, 7, 8, null).map(Values::new)
                .sorted(safeComparing(Values::value, 100).reversed()).toList();
        assertEquals(8, values.get(3).value());
    }

    @Test
    void sortSecureWithLowDefaultValue() {
        List<Values> values = Stream.of(1, 2, null, 4, 5, null, 7, 8, null).map(Values::new)
                .sorted(safeComparing(Values::value, -1).reversed()).toList();
        assertEquals(4, values.get(3).value());
    }
}
