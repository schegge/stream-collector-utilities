package de.schegge.collector;

import static de.schegge.collector.EllipsisCollector.ellipsis;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

class EllipsisCollectorTest {

  @Test
  void ellipsisWithParameters() {
    assertEquals("a; b; c; d", Stream.of("a", "b", "c", "d").collect(ellipsis("; ", ">>>", 16)));
    assertEquals("a; b; c; d; e; >>>",
        Stream.of("a", "b", "c", "d", "e", "f", "g").collect(ellipsis("; ", ">>>", 16)));
    assertTrue(Stream.<String>empty().collect(ellipsis(10)).isEmpty());
  }

  @Test
  void ellipsisWithDefaults() {
    assertEquals("a, b, c, d", Stream.of("a", "b", "c", "d").collect(ellipsis(16)));
    assertEquals("a, b, c, d, e, f, …",
        Stream.of("a", "b", "c", "d", "e", "f", "g").collect(ellipsis(16)));
    assertTrue(Stream.<String>empty().collect(ellipsis(10)).isEmpty());
  }
}