package de.schegge.collector;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Map;
import org.junit.jupiter.api.Test;

class EnumUtilTest {

  private enum Key {
    A, B,C
  }

  private enum Value {
    A, B,C
  }

  private enum MoreValue {
    A, B, C, D
  }

  private enum LessValue {
    A, B
  }

  @Test
  void createMap() {
    Map<Key, Value> map = EnumUtil.mapping(Key.class, Value.class);
    assertAll(
        () -> assertEquals(Value.A, map.get(Key.A)),
        () -> assertEquals(Value.B, map.get(Key.B)),
        () -> assertEquals(Value.C, map.get(Key.C))
    );
  }

  @Test
  void createMapWithDifferentCardinality() {
    assertThrows(IllegalArgumentException.class, () -> EnumUtil.mapping(Key.class, LessValue.class));
    assertThrows(IllegalArgumentException.class, () -> EnumUtil.mapping(Key.class, MoreValue.class));
  }

  @Test
  void createMapWithValuesWithSameCardinality() {
    Map<Key, Value> map = EnumUtil.mapping(Key.class, Value.A, Value.B, Value.C);
    assertAll(
        () -> assertEquals(Value.A, map.get(Key.A)),
        () -> assertEquals(Value.B, map.get(Key.B)),
        () -> assertEquals(Value.C, map.get(Key.C))
    );
  }

  @Test
  void createMapWithValuesWithLowerCardinality() {
    Map<Key, LessValue> map = EnumUtil.mapping(Key.class, LessValue.A, LessValue.B, LessValue.B);
    assertAll(
        () -> assertEquals(LessValue.A, map.get(Key.A)),
        () -> assertEquals(LessValue.B, map.get(Key.B)),
        () -> assertEquals(LessValue.B, map.get(Key.C))
    );
  }

  @Test
  void createMapWithValuesWithHigherCardinality() {
    Map<Key, MoreValue> map = EnumUtil.mapping(Key.class, MoreValue.A, MoreValue.B, MoreValue.D);
    assertAll(
        () -> assertEquals(MoreValue.A, map.get(Key.A)),
        () -> assertEquals(MoreValue.B, map.get(Key.B)),
        () -> assertEquals(MoreValue.D, map.get(Key.C))
    );
  }
}