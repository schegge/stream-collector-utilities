package de.schegge.collector;

import static de.schegge.collector.EnumeratedCollector.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

class EnumeratedCollectorTest {

  @Test
  void enumerate() {
    assertEquals("a, b, c und d", Stream.of("a", "b", "c", "d").collect(enumerated()));
    assertEquals("a und b", Stream.of("a", "b").collect(enumerated()));
    assertEquals("a", Stream.of("a").collect(enumerated()));
  }

  @Test
  void enumerateWithLastDelimiter() {
    assertEquals("a, b, c oder d", Stream.of("a", "b", "c", "d").collect(enumerated(" oder ")));
    assertEquals("a oder b", Stream.of("a", "b").collect(enumerated(" oder ")));
    assertEquals("a", Stream.of("a").collect(enumerated(" oder ")));
  }

  @Test
  void enumerateWithLastDelimiterAndDelimiter() {
    assertEquals("a or b or c or d", Stream.of("a", "b", "c", "d").collect(enumerated(" or ", " or ")));
    assertEquals("a or b", Stream.of("a", "b").collect(enumerated(" or ", " or ")));
    assertEquals("a", Stream.of("a").collect(enumerated(" or ", " or ")));
  }

  @Test
  void prefixedEnumerate() {
    assertEquals("list: a, b, c und d",
        Stream.of("a", "b", "c", "d").collect(prefixedEnumerated("list: ")));
    assertEquals("list: a oder b", Stream.of("a", "b").collect(prefixedEnumerated("list: ", " oder ")));
    assertEquals("list: a", Stream.of("a").collect(prefixedEnumerated("list: ")));
  }
}