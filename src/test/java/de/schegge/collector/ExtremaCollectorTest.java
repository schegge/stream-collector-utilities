package de.schegge.collector;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.Month;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

class ExtremaCollectorTest {

  record Person(LocalDate birthDay) {

  }

  @Test
  void minmax() {
    Extrema<Integer> extrema = IntStream.range(1, 100000000).parallel().boxed().collect(ExtremaCollector.extrema(Comparator.<Integer>naturalOrder()));
    assertEquals(99999999, extrema.getMax().orElse(0));
    assertEquals(1, extrema.getMin().orElse(0));
  }

  @Test
  void birthdaysParallel() {
    List<Person> persons = IntStream.range(1900, 2020).parallel().mapToObj(i -> LocalDate.of(i, Month.AUGUST, 24)).map(
        Person::new).toList();

    Extrema<Person> extrema = persons.stream().collect(ExtremaCollector.extrema(Comparator.comparing(Person::birthDay)));
    assertEquals(LocalDate.of(2019, Month.AUGUST, 24), extrema.getMax().map(Person::birthDay).orElse(null));
    assertEquals(LocalDate.of(1900, Month.AUGUST, 24), extrema.getMin().map(Person::birthDay).orElse(null));
  }

  @Test
  void birthdays() {
    List<Person> persons = IntStream.range(1900, 2020).mapToObj(i -> LocalDate.of(i, Month.AUGUST, 24)).sorted(Comparator.<LocalDate>naturalOrder().reversed()).map(
        Person::new).toList();

    Extrema<Person> extrema = persons.stream().collect(ExtremaCollector.extrema(Comparator.comparing(Person::birthDay)));
    assertEquals(LocalDate.of(2019, Month.AUGUST, 24), extrema.getMax().map(Person::birthDay).orElse(null));
    assertEquals(LocalDate.of(1900, Month.AUGUST, 24), extrema.getMin().map(Person::birthDay).orElse(null));
  }

  @Test
  void birthdaysReverse() {
    List<Person> persons = IntStream.range(1900, 2020).map(y -> 3919 - y).mapToObj(i -> LocalDate.of(i, Month.AUGUST, 24)).sorted(Comparator.<LocalDate>naturalOrder().reversed()).map(
        Person::new).toList();

    Extrema<Person> extrema = persons.stream().collect(ExtremaCollector.extrema(Comparator.comparing(Person::birthDay)));
    assertEquals(LocalDate.of(2019, Month.AUGUST, 24), extrema.getMax().map(Person::birthDay).orElse(null));
    assertEquals(LocalDate.of(1900, Month.AUGUST, 24), extrema.getMin().map(Person::birthDay).orElse(null));
  }
}
