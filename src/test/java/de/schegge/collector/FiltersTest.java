package de.schegge.collector;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

class FiltersTest {

  private static final List<Pojo> POJOS_WITH_EMPTY = List.of(new Pojo("1"), new Pojo(""), new Pojo("3"));
  private static final List<Pojo> POJOS_WITH_NULL = List.of(new Pojo("1"), new Pojo(null), new Pojo("3"));

  private static final class Pojo {

    final String value;

    private Pojo(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }
  }

  @Test
  void testIsNonNull() {
    List<Pojo> list = POJOS_WITH_NULL.stream().filter(Filters.nonNull(Pojo::getValue)).toList();
    assertAll(
        () -> assertEquals(2, list.size()),
        () -> assertEquals("1", list.get(0).getValue()),
        () -> assertEquals("3", list.get(1).getValue())
    );
  }

  @Test
  void testIsNull() {
    List<Pojo> list = POJOS_WITH_NULL.stream().filter(Filters.isNull(Pojo::getValue)).toList();
    assertAll(
        () -> assertEquals(1, list.size()),
        () -> assertNull(list.get(0).getValue())
    );
  }

  @Test
  void testIsEmpty() {
    List<Pojo> list = POJOS_WITH_EMPTY.stream().filter(Filters.isEmpty(Pojo::getValue)).toList();
    assertAll(
        () -> assertEquals(1, list.size()),
        () -> assertEquals("", list.get(0).getValue())
    );
  }

  @Test
  void testNonEmpty() {
    List<Pojo> list = POJOS_WITH_EMPTY.stream().filter(Filters.nonEmpty(Pojo::getValue)).toList();
    assertAll(
        () -> assertEquals(2, list.size()),
        () -> assertEquals("1", list.get(0).getValue()),
        () -> assertEquals("3", list.get(1).getValue())
    );
  }

  @Test
  void testNonEmptyString() {
    List<String> list = Stream.of("abc", "123", "  ", "").filter(Filters.nonEmpty()).collect(toList());
    assertEquals(List.of("abc", "123", "  "), list);
  }

  @Test
  void testNonBlankString() {
    List<String> list = Stream.of("abc", "123", "  ", "").filter(Filters.nonBlank()).collect(toList());
    assertEquals(List.of("abc", "123"), list);
  }
}
