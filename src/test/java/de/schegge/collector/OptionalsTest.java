package de.schegge.collector;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class OptionalsTest {
	@Test
	void collectToListWithoutEmpty() {
		List<Optional<String>> list = asList(Optional.of("test1"), Optional.of("test2"), Optional.empty(),
				Optional.of("test2"), Optional.empty());

		List<String> values = list.stream().collect(OptionalsCollector.withoutEmpty(toList()));
		assertEquals(asList("test1", "test2", "test2"), values);
	}

	@Test
	void collectToListWithEmpty() {
		List<Optional<String>> list = asList(Optional.of("test1"), Optional.of("test2"), Optional.empty(),
				Optional.of("test2"), Optional.empty());

		List<String> values = list.stream().collect(OptionalsCollector.withEmpty(toList()));
		assertEquals(asList("test1", "test2", null, "test2", null), values);
	}

	@Test
	void collectToSetWithoutEmpty() {
		List<Optional<String>> list = asList(Optional.of("test1"), Optional.of("test2"), Optional.empty(),
				Optional.of("test2"), Optional.empty());

		Set<String> values = list.stream().collect(OptionalsCollector.withoutEmpty(toSet()));
		assertEquals(new HashSet<>(asList("test1", "test2")), values);
	}
}