package de.schegge.collector;

import static de.schegge.collector.PortionsCollector.toLists;
import static de.schegge.collector.PortionsCollector.toSets;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class PortionTest {

	@Test
	void collectToLists() {
		assertIterableEquals(asList(asList("a", "b"), asList("c", "d")),
				Stream.of("a", "b", "c", "d").collect(toLists(2)));
	}

	@Test
	void collectToSets() {
		Collection<Set<String>> actual = Stream.of("a", "b", "c", "d").collect(toSets(2));
		assertTrue(actual.contains(new HashSet<>(asList("a", "b"))));
		assertTrue(actual.contains(new HashSet<>(asList("c", "d"))));
	}

	@Test
	void parallel() {
		Stream<String> stream = Stream.of("a", "b", "c", "d");
		assertThrows(UnsupportedOperationException.class,
				() -> stream.parallel().collect(toLists(2)));
	}

}
