package de.schegge.collector;

import de.schegge.collector.RangesCollector.Compactness;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RangesCollectorTest {

    @Test
    void ranges() {
        List<Integer> list = List.of(1101, 1102, 1103, 1105, 1106, 1107, 1110, 1120, 1121, 1122, 1123);
        assertEquals("1101-1103,1105-1107,1110,1120-1123", list.stream().collect(RangesCollector.ranges()));
        assertEquals("1101+2,1105+2,1110,1120+3", list.stream().collect(RangesCollector.ranges(Compactness.OFFSET)));
        assertEquals("1101+2,+2+2,+3,+10+3", list.stream().collect(RangesCollector.ranges(Compactness.RANGE_OFFSET)));
    }
}